-- Create all tables
CREATE TABLE login (
    id     SERIAL PRIMARY KEY,
    usuario  VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    id_google VARCHAR
);

CREATE TABLE files (
    id_usuario SERIAL PRIMARY KEY,
    image BYTEA,
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES login(id) ON DELETE CASCADE
);

CREATE TABLE reporte (
    id    SERIAL PRIMARY KEY,
    id_usuario    int NOT NULL,
    calle    varchar(50),
    ciudad varchar(50),
    latitude double precision,
    longitude double precision,
    comentario varchar(150),
    anonimo boolean,
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES login(id) ON DELETE CASCADE
);


CREATE TABLE lugar_etiquetado (
    id     SERIAL PRIMARY KEY,
    id_usuario int NOT NULL,
    lugar     VARCHAR(50),
    calle    VARCHAR(50),
    latitude double precision,
    longitude double precision,
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES login(id) ON DELETE CASCADE
);

CREATE TABLE lugar_favorito (
    id SERIAL PRIMARY KEY,
    id_usuario int NOT NULL,
    calle    VARCHAR(50),
    latitude double precision,
    longitude double precision,
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES login(id) ON DELETE CASCADE
);

CREATE TABLE comentario_reporte (
    id SERIAL PRIMARY KEY,
    id_reporte int NOT NULL,
    id_usuario int NOT NULL,
    comentario VARCHAR(200) NOT NULL,
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES login(id) ON DELETE CASCADE
);

-- Insert into login
-- INSERT INTO login (usuario, email, password) VALUES ('juanmi', 'jmseguraf01@elpuig.xeill.net', 'juanmi123');
-- INSERT INTO login (usuario, email, password) VALUES ('ivan', 'imirallesg01@elpuig.xeill.net', 'ivan123');

-- Insert into reportes
-- INSERT INTO reporte (id_usuario, calle, latitude, longitude, comentario) VALUES (1, 'Av. america', 45667543, 54345678, 'Me han robado.');
-- INSERT INTO reporte (id_usuario, calle, latitude, longitude, comentario) VALUES (2, 'Av. san jaume', 09876567, 9765678, 'Sin comentarios.');

-- Insert into lugares etiquetados
-- INSERT INTO lugar_etiquetado (id_usuario, lugar, calle, latitude, longitude) VALUES (1, 'Casa', 'C/ Casa', 7654432, 7654324);
-- INSERT INTO lugar_etiquetado (id_usuario, lugar, calle, latitude, longitude) VALUES (2, 'Colegio', 'C/ Colegio', 343342, 432432);

-- Inseret into lugares favoritos
-- INSERT INTO lugar_favorito (id_usuario, calle, latitude, longitude) VALUES (1, 'C/ Ejemplo', 432432, 432432);
-- INSERT INTO lugar_favorito (id_usuario, calle, latitude, longitude) VALUES (2, 'C/ Ejemplo2', 432432, 432432);
