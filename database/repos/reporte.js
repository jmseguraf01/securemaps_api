class Reporte {
  constructor(db, pgp) {
    this.db = db;
  }

  // Añade reporte
  add(idUsuario, calle, ciudad, latitude, longitude, comentario, anonimo) {
    return this.db.result("INSERT INTO reporte (id_usuario, calle, ciudad, latitude, longitude, comentario, anonimo) VALUES ($1, $2, $3, $4, $5, $6, $7)", [idUsuario, calle, ciudad, latitude, longitude, comentario, anonimo], result => result.rowCount);
  }

  // Obtiene reportes ordenados por id
  get(idUsuario) {
      return this.db.any("SELECT * FROM reporte where id_usuario = $1 order by id DESC", [idUsuario]);
  }

  // Obtiene todos los reportes
  getAll() {
    return this.db.any("SELECT * FROM reporte");
  }

  // Obtiene reportes consultando la calle
  getWithCalle(idUsuario, calle) {
    return this.db.any("SELECT * FROM reporte WHERE LOWER(calle) LIKE $2 AND id_usuario = $1", [idUsuario, '%' +calle+'%']);
  }

  // Obtiene reportes consultando la ciudad
  getWithCiudad(ciudad) {
    return this.db.any("SELECT * FROM reporte WHERE ciudad = $1", [ciudad]);
  }

  // Actualiza reportes
  update(idReporte, calle, ciudad, latitude, longitude, comentario) {
    return this.db.result("UPDATE reporte set calle = $2, ciudad = $3, latitude = $4, longitude = $5, comentario = $6 where id = $1", [idReporte, calle, ciudad, latitude, longitude, comentario], result => result.rowCount);
  }

  // Elimina reportes
  delete(idReporte) {
    return this.db.result("DELETE FROM reporte WHERE id = $1", [idReporte], result => result.rowCount);
  }
}

module.exports = Reporte;
