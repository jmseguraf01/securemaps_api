class LugaresFavoritos {
  constructor(db, pgp) {
    this.db = db;
  }

  // Obtiene lugares favoritos
  get(idUsuario) {
    return this.db.any("SELECT * FROM lugar_favorito where id_usuario = $1 order by id", [idUsuario]);
  }

  // Inserta lugar favorito
  insert(idUsuario, calle, latitude, longitude) {
    return this.db.result("INSERT INTO lugar_favorito(id_usuario, calle, latitude, longitude) VALUES($1, $2, $3, $4)", [idUsuario, calle, latitude, longitude], result => result.rowCount);
  }

  // Actualiza lugares favoritos
  update(id, calle, latitude, longitude) {
    return this.db.result("UPDATE lugar_favorito SET calle = $2, latitude = $3, longitude = $4 WHERE id = $1", [id, calle, latitude, longitude], result => result.rowCount);
  }

  // Elimina lugares favoritos
  delete(id) {
    return this.db.result("DELETE FROM lugar_favorito WHERE ID = $1", [id], result => result.rowCount);
  }

}

module.exports = LugaresFavoritos;
