class LugaresEtiquetados {
  constructor(db, pgp) {
    this.db = db;
  }

  // Añade los lugares etiquetados por defecto
  addLugaresDefecto(idUsuario) {
    return this.db.result("INSERT INTO lugar_etiquetado(id_usuario, lugar) values ($1, 'Casa'), ($1, 'Trabajo'), ($1, 'Gimnasio'), ($1,'Iglesia'), ($1, 'Escuela')", [idUsuario], result => result.rowCount);
  }

  // Obtiene lugares etiquetados ordenados por el id
  get(idUsuario) {
    return this.db.any("SELECT * FROM lugar_etiquetado where id_usuario = $1 order by id", [idUsuario]);
  }

  // Actualiza lugares etiquetados
  update(id, calle, latitude, longitude) {
    return this.db.result("UPDATE lugar_etiquetado set calle = $2, latitude = $3, longitude = $4 WHERE id = $1", [id, calle, latitude, longitude], result => result.rowCount);
  }

  // Elimina lugares etiquetados
  delete(id) {
    return this.db.result("UPDATE lugar_etiquetado set calle = null, latitude = 0, longitude = 0 WHERE id = $1", [id], result => result.rowCount);
  }

  getAllUserAndHouse() {
    return this.db.any("select * from lugar_etiquetado where lugar = 'Casa' AND latitude != 0 AND LONGITUDE != 0");
  }

}

module.exports = LugaresEtiquetados;
