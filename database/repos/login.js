class Login {
    constructor(db, pgp) {
        this.db = db;
    }

  // Autentica en el login
  async auth(email, password, pwd_encriptado) {
    let res;
    try {
      if (pwd_encriptado) {
        // El password se envia encriptado
        res = await this.db.one("SELECT * FROM login WHERE email = $1 AND password = $2 LIMIT 1", [email, password]);
      }
      // El password se envia sin encriptar
      else if (!pwd_encriptado) {
        res = await this.db.one("SELECT * FROM login WHERE email = $1 AND password = crypt($2, password) LIMIT 1", [email, password]);
      }
    } catch(e){
      res = {};
    }
    return res;
  }

  // Autentica con google, comprobando el email y el id
  authGoogle(email, id_google) {
    return this.db.one("SELECT * FROM login WHERE email = $1 AND id_google = $2", [email, id_google]);
  }

  // Añade usuario
  add(usuario, email, password, id_google, notificar_reporte) {
    return this.db.result("INSERT INTO login (usuario, email, password, id_google, notificar_reporte) VALUES ($1, $2, crypt($3, gen_salt('bf', 8)), $4, $5)", [usuario, email, password, id_google, notificar_reporte], result => result.rowCount);
  }

  // Actualiza usuario
  update(id, usuario, email) {
    return this.db.result("UPDATE login set usuario = $2, email = $3 where id = $1", [id, usuario, email], result => result.rowCount);
  }

  // Actualiza password
  updatePassword(idUsuario, password) {
    return this.db.result("UPDATE login set password = crypt($2, gen_salt('bf', 8)) where id = $1", [idUsuario, password], result => result.rowCount);
  }

  // Actualiza password por el correo
  updatePasswordWithEmail(email, password) {
    return this.db.result("UPDATE login set password = crypt($2, gen_salt('bf', 8)) where email = $1", [email, password], result => result.rowCount);
  }

  // Actualiza la notificacion de los reportes
  updateNotificacionReporte(id, notificacion) {
    return this.db.result("UPDATE login set notificar_reporte = $2 where id = $1", [id, notificacion], result => result.rowCount);
  }

  // Inserto la imagen del usuario
  insertImage(idUsuario, imagen) {
    return this.db.result("INSERT INTO files VALUES($1, $2)", [idUsuario, imagen], result => result.rowCount);
  }

  // Actualiza la imagen del usuario
  updateImage(idUsuario, imagen) {
    return this.db.result("UPDATE files set image = $2 where id_usuario = $1", [idUsuario, imagen], result => result.rowCount);
  }

  // Elimina usuario
  delete(id) {
    return this.db.result("DELETE FROM login WHERE id = $1", [id], result => result.rowCount);
  }

  // Comprueba si el correo existe en la base de datos
  comprobeEmail(email) {
    return this.db.oneOrNone("SELECT * FROM login WHERE email = $1", [email]);
  }

  // Obtengo el campo de notificar incidente
  getNotificarReportes(id) {
    return this.db.oneOrNone("SELECT notificar_reporte FROM login WHERE id = $1", [id]);
  }

  // Devuelve el usuario
  getUserWithID(idUsuario) {
    return this.db.oneOrNone("SELECT * FROM login WHERE id = $1", [idUsuario]);
  }

  // Comprueba si el usuario tiene imagen
  comprobeUserImage(idUsuario) {
    return this.db.any("SELECT id_usuario FROM files WHERE id_usuario = $1", [idUsuario]);
  }

  // Devuelve la imagen del usuario
  getUserImage(idUsuario) {
    return this.db.any("SELECT image FROM files WHERE id_usuario = $1", [idUsuario]);
  }

}

module.exports = Login;
