class ComentarioReporte {
  constructor(db, pgp) {
    this.db = db;
  }

  // Añade un nuevo comentario a un reporte
  add(idReporte, idUsuario, comentario) {
    return this.db.result("INSERT INTO comentario_reporte (id_reporte, id_usuario, comentario) VALUES ($1, $2, $3)", [idReporte, idUsuario, comentario], result => result.rowCount);
  }

  // Obtiene todos los comentarios
  get(idReporte, idUsuario) {
    return this.db.any("SELECT * FROM comentario_reporte WHERE id_reporte = $1 ORDER BY id", [idReporte, idUsuario]);
  }

  // Elimina el comentario
  delete(idReporte) {
    return this.db.result("DELETE FROM comentario_reporte WHERE id = $1", [idReporte], result => result.rowCount);
  }

  // Actualiza el comentario
  update(id, comentario) {
    return this.db.result("UPDATE comentario_reporte SET comentario = $2 where id = $1", [id, comentario], result => result.rowCount);
  }

}

module.exports = ComentarioReporte;
