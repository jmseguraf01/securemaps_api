const promise = require("bluebird");
const pgPromise = require("pg-promise");
const Login = require("./repos/login");
const Reporte = require("./repos/reporte");
const LugaresEtiquetados = require("./repos/lugares_etiquetados");
const LugaresFavoritos = require("./repos/lugares_favoritos");
const ComentarioReporte = require("./repos/comentario_reporte");
// Para leer la variable de entorno con .env
const dotenv = require('dotenv');
dotenv.config();

const initOptions = {
  promiseLib: promise,
  extend(obj, dc) {
    obj.login = new Login(obj, pgp);
    obj.reporte = new Reporte(obj, pgp);
    obj.lugaresEtiquetados = new LugaresEtiquetados(obj, pgp);
    obj.lugaresFavoritos = new LugaresFavoritos(obj, pgp);
    obj.comentarioReporte = new ComentarioReporte(obj, pgp);
  }
};


const pgp = pgPromise(initOptions);
const db = pgp({
  connectionString: process.env.DATABASE_URL,
  ssl: process.env.DATABASE_URL ? {rejectUnauthorized: false} : false
});

module.exports = db;
