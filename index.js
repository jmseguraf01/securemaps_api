const express = require('express');
const path = require('path');
const session = require('express-session');
const db = require("./database/index_db");
const crypto = require('crypto-js');
const bodyParser = require("body-parser");
const fs = require('fs');
const https = require('https');
const nodemailer = require('nodemailer');
const multer = require('multer')
const crypto2 = require('crypto');
const { v4: uuidv4,} = require('uuid');
const app = express();
const errorCode = 204;
const PORT = 8444;

// Para usar el body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Para crear la sesion con HTTPS
https.createServer({
  key: fs.readFileSync('../certs/privkey.pem'),
  cert: fs.readFileSync('../certs/fullchain.pem')
}, app).listen(PORT, function(){
  console.log("My HTTPS server listening on port " + PORT + "...");
});

// Metodo para enviar mensaje por correo
async function getTransport()  {
    return nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.EMAIL,
          pass: process.env.PASSWORD
        }
    });
}
const gettransport = getTransport();

// Comprueba si el usuario esta logeado
async function restrict(req, res, next) {
    if(user = await db.login.auth(req.query.email, req.query.password, true)) {
      // Login correcto
      if (user.email != null && user.password != null) {
        usuario_id = user.id;
        next();
      }
      // Login incorrecto
      else {
        res.send(401);
      }
    } else {
      res.send(401);
    }
}


// Hace login
app.get('/login', async (req, res) => {
    if(user = await db.login.auth(req.query.email, req.query.password, false)) {
      // Si es incorrecto, lo intento con el password encriptado
      if (user.email == null && user.password == null) {
        user = await db.login.auth(req.query.email, req.query.password, true);
      }
      // Login correcto
      if (user.email != null && user.password != null) {
        res.status(200).send({id: user.id, usuario: user.usuario, email: user.email, password: user.password, notificarReporte: user.notificar_reporte});
      }
      // Login incorrecto
      else {
          res.send(401);
      }
    }
});

// Hace login con google
app.get('/loginGoogle', async (req, res) => {
    if(user = await db.login.authGoogle(req.query.email, req.query.id_google)) {
      // Login correcto
      if (user.email != null) {
        res.status(200).send({id: user.id, usuario: user.usuario, email: user.email, password: user.password, notificarReporte: user.notificar_reporte});
      }
      // Login incorrecto
      else {
          res.send(401);
      }
    }
});

// Recuperar contraseña
app.post('/resetpassword', async (req, res) => {
  // Siempre que el email exista en la base de datos
  if (user = await db.login.comprobeEmail(req.body.nuevo_email)) {
    const transporter = await gettransport;
    // Creo una contraseña
    var randomstring = Math.random().toString(36).slice(-12);
    // URL Y Encrypto el mail
    url = "https://api.securemaps.es:8444/verify_resetpassword"
    var cipher = crypto2.createCipher('aes-256-ecb', process.env.PASSWORD_CRYPT);
    var encryptedEmail = cipher.update(req.body.nuevo_email, 'utf8', 'hex') + cipher.final('hex');
    // Formo el correo al enviar
    var mailOptions = {
      from: process.env.EMAIL,
      to: req.body.nuevo_email,
      subject: 'Nueva contraseña Securemaps',
      html: `Tu nueva contraseña para iniciar sesion en Securemaps es: ${randomstring}. <a href='${url}/?nuevo_email=${encryptedEmail}&password=${randomstring}'>Haz click aquí para confirmar el cambio de contraseña.</a>`
    };
    // Envio el correo
    const info = await transporter.sendMail(mailOptions, (error, info) => {
      // Si no ha habido error
      if (!error) {
        // CAMBIAR PASSWORD EN LA DB - Comprobar con alguna url
        res.send(200);
      }
      // Si hay un error
      else {
        res.send(400);
      }
    });
  }
  // El email no existe
  else {
    res.send(404);
  }
});

// Restablece la contraseña del usuario
app.get('/verify_resetpassword', async (req, res) => {
  try {
    var cipher = crypto2.createDecipher('aes-256-ecb', process.env.PASSWORD_CRYPT);
    var email = cipher.update(req.query.nuevo_email, 'hex', 'utf8') + cipher.final('utf8');
    // Actualizo la contraseña de la cuenta
    result = await db.login.updatePasswordWithEmail(email, req.query.password);
    // Password cambiada
    if (result > 0) {
      res.send(200);
    } else {
      res.send(400);
    }
  } catch (e) {
      res.send(400);
  }
});

// Añade un usuario
app.post("/addUser", async (req, res) => {
  try {
    result = await db.login.add(req.body.nuevo_usuario, req.body.nuevo_email, req.body.nuevo_password, req.body.id_google, req.body.notificar_reporte);
    // Si se ha registado bien
    if(result > 0) {
      // Se hace login y se envia los datos a la aplicacion
      user = await db.login.auth(req.body.nuevo_email, req.body.nuevo_password, false);
      if (user.email != null && user.password != null) {
        res.send({id: user.id, usuario: user.usuario, email: user.email, password: user.password});
      } else {
        res.send(401);
      }
    }
    // Error al registrar
    else {
      res.send(400);
    }
  } catch (e) {
    // Error al realizar la operacion en la base de datos
    console.log(e);
    res.status(200).send({ error: e.code});
  }
});

// Obtiene el usuario dando el id
app.get("/getUsername", restrict, async(req, res) =>  {
  if (user = await db.login.getUserWithID(req.query.id)) {
    // res.status(200).send({usuario: user.usuario, email: user.email});
    res.status(200).send({id: user.id, usuario: user.usuario, email: user.email, password: user.password});
  }
  // No existe el usuario
  else {
    res.send(400);
  }
});

// Actualiza los datos del usuario
app.post("/updateUser", restrict, async (req, res) => {
      result = await db.login.update(usuario_id, req.body.nuevo_usuario, req.body.nuevo_email);
      if (result > 0) {
        res.send(200);
      } else {
        res.send(400);
      }
});

// Actualiza la contraseña del usuario
app.post("/updateUserPassword", restrict, async (req, res) => {
      result = await db.login.updatePassword(usuario_id, req.body.nuevo_password);
      if (result > 0) {
        res.send(200);
      } else {
        res.send(400);
      }
});

// Actualiza la notificacion de los reportes
app.post("/updateNotificacionReporte", restrict, async (req, res) => {
  result = await db.login.updateNotificacionReporte(usuario_id, req.body.notificar_reporte);
  if (result > 0) {
    res.send(200);
  } else {
    res.send(400);
  }
});

// Elimina al usuario y toda la informacion
app.get("/deleteUser", restrict, async (req, res) => {
      result = await db.login.delete(usuario_id);
      // Eliminado con exito
      if (result > 0) {
        res.send(200);
      }
      // Error al eliminar
      else {
        res.send(400);
      }
});

// Añade reporte
app.post("/addReporte", restrict, async (req, res) => {
      result = await db.reporte.add(usuario_id, req.body.calle, req.body.ciudad, req.body.latitude, req.body.longitude, req.body.comentario, req.body.anonimo);
      // Añadido con exito
      if (result > 0) {
        res.send(200);
        notificarReportes(req.body.latitude, req.body.longitude, req.body.calle);
      }
      // Error al añadir
      else {
        console.log("Error al añadir");
        res.send(400);
      }
});

// Obtiene los reportes de un usuario
app.get('/getReportes', restrict, async (req, res) => {
  res.send({results: await db.reporte.get(usuario_id)});
});

// Obtiene todos los reportes de todos los usuarios
app.get('/getAllReportes', restrict, async (req, res) => {
  res.send({results: await db.reporte.getAll()});
});


// Obtiene los reportes por calle (importante recibir la calle en minuscula)
app.get('/getReportesWithCalle', restrict, async (req, res) => {
  res.send({results: await db.reporte.getWithCalle(usuario_id, req.query.calle)});
});

// Obtiene los reportes por la ciudad
app.get('/getReportesWithCiudad', restrict, async (req, res) => {
  res.send({results: await db.reporte.getWithCiudad(req.query.ciudad)});
});

// Actualiza reporte
app.post('/updateReportes', restrict, async (req, res) => {
  result = await db.reporte.update(req.body.id, req.body.calle, req.body.ciudad, req.body.latitude, req.body.longitude, req.body.comentario);
  // Actualizado con exito
  if (result > 0) {
    res.send(200);
  }
  // Error al actualizar
  else {
    console.log("Error al actualizar.")
    res.send(400);
  }
});

// Elimina reporte
app.get('/deleteReportes', restrict, async (req, res) => {
  result = await db.reporte.delete(req.query.id);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  }
  // Error al eliminar
  else {
    console.log("Error al eliminar");
    res.send(400);
  }
});

// Inserta los luages etiqutados por defecto
app.post('/insertLugares-etiquetados', restrict, async (req, res) => {
  result = await db.lugaresEtiquetados.addLugaresDefecto(usuario_id);
  // Se han insertado con exito
  if (result == 5) {
    res.send(200);
  }
  // Error al insertar
  else {
    res.send(400);
  }
});

// Obtiene los lugares etiquetados
app.get('/getlugares-etiquetados', restrict, async (req, res) => {
  res.send({results: await db.lugaresEtiquetados.get(usuario_id)});
});

// Actualiza los lugares etiquetados
app.post('/updatelugares-etiquetados', restrict, async (req, res) => {
  result = await db.lugaresEtiquetados.update(req.body.id, req.body.calle, req.body.latitude, req.body.longitude);
  // Actualizado con exito
  if (result > 0) {
    res.send(200);
  } else {
    res.send(400);
  }
});

// Elimina el lugar etiquetado
app.get('/deletelugares-etiquetados', restrict, async (req, res) => {
  result = await db.lugaresEtiquetados.delete(req.query.id);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  } else {
    res.send(400);
  }
});


// Crear nuevo lugar favotito
app.post('/insertlugares-favoritos', restrict, async (req, res) => {
  // result = await db.lugaresFavoritos.insert(usuario_id, req.query.calle, req.query.latitude, req.query.longitude);
  result = await db.lugaresFavoritos.insert(usuario_id, req.body.calle, req.body.latitude, req.body.longitude);
  // Insertado con exito
  if (result > 0) {
    res.send(200);
  }
  // Error al insertar
  else {
    res.send(400);
  }
});

// Obtiene los lugares favoritos
app.get('/getlugares-favoritos', restrict, async (req, res) => {
  res.send({results: await db.lugaresFavoritos.get(usuario_id)});
});


// Actualiza los lugares favoritos
app.post('/updatelugares-favoritos', restrict, async (req, res) => {
  result = await db.lugaresFavoritos.update(req.body.id, req.body.calle, req.body.latitude, req.body.longitude);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  }
  // Error al eliminar
  else {
    res.send(400);
  }
});


// Elimina el lugar favorito
app.get('/deletelugares-favoritos', restrict, async (req, res) => {
  result = await db.lugaresFavoritos.delete(req.query.id);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  }
  // Error al eliminar
  else {
    res.send(400);
  }
});

// Añade un comentario a un reporte
app.post("/addComentarioReporte", restrict, async (req, res) => {
  result = await db.comentarioReporte.add(req.body.id_reporte, usuario_id, req.body.comentario);
  // Comentario agregado
  if (result > 0) {
    res.send(200);
  }
  // Error al añadir el comentario
  else {
    res.send(400);
  }
});

// Obtiene todos los comentarios de un reporte
app.get("/getComentariosReporte", restrict, async (req, res) => {
  res.send({results: await db.comentarioReporte.get(req.query.id_reporte)});
});

// Eliminar el comentario de un reporte
app.post("/deleteComentarioReporte", restrict, async (req, res) => {
  result = await db.comentarioReporte.delete(req.body.id);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  } else {
    res.send(400);
  }
});


// Modificar el comentario de un reporte
app.post("/updateComentarioReporte", restrict, async (req, res) => {
  result = await db.comentarioReporte.update(req.body.id, req.body.comentario);
  // Eliminado con exito
  if (result > 0) {
    res.send(200);
  } else {
    res.send(400);
  }
});

// Storage de la imagen a guardar
storage = multer.diskStorage({
  destination: './uploads/',
  filename: function(req, file, cb) {
    return crypto2.pseudoRandomBytes(16, function(err, raw) {
      if (err) {
        return cb(err);
      }
      return cb(null, "" + (raw.toString('hex')) + (path.extname(file.originalname)));
    });
  }
});


// Subo la imagen del usuario
app.post("/upload", multer({storage: multer.memoryStorage()}).single('avatar'), restrict, async (req, res) => {
    // Obtengo si ese usuario tiene imagen
    exist = await db.login.comprobeUserImage(usuario_id);
    if (exist[0] == null) {
      console.log("Insert");
      result = await db.login.insertImage(usuario_id, req.file.buffer);
      if (result > 0) {
        res.send(200);
      } else {
        res.send(400);
      }
      // Envio la respuesta
    } else {
      console.log("Update");
      result = await db.login.updateImage(usuario_id, req.file.buffer);
      if (result > 0) {
        res.send(200);
      } else {
        res.send(400);
      }
    }


});

// Obtengo la imagen pasandole el id del usuario
app.get('/uploads/:idUsuario', async(req, res) => {
  const result = await db.login.getUserImage(req.params.idUsuario);
  try {
    const img = Buffer.from(result[0].image, 'binary');
    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');
  } catch (e) {
    res.send(400);
  }
});

// Obtengo la imagen del usuario
app.get("/getUserImage", restrict, async (req, res) => {
  result = await db.login.getUserImage(usuario_id);
  res.send({imageUrl: result.image});
});


// Notifica los reportes cercanos a la casa del usuario
async function notificarReportes(latitude, longitude, calle) {
  lugares = await db.lugaresEtiquetados.getAllUserAndHouse();
  // Miro cada reporte y cada casa de cada usuario
  for (let lugar of lugares) {
    latituds = lugar.latitude - latitude;
    longituds = lugar.longitude - longitude;
    distancia = Math.sqrt(Math.pow(latituds, 2) + Math.pow(longituds, 2));
    // Si el reporte esta cerca de la casa, envio mail
    if (distancia <= 0.001) {
      notificar = await db.login.getNotificarReportes(lugar.id_usuario);
      if (notificar.notificar_reporte == true) {
        console.log("Notifico por correo el reporte creado")
        user = await db.login.getUserWithID(lugar.id_usuario);
        const transporter = await gettransport;
        // Formo el correo al enviar
        var mailOptions = {
          from: process.env.EMAIL,
          to: user.email,
          subject: 'Incidente reportado cerca de tu vivienda - Securemaps',
          html: `Atencion! Se ha reportado un incidente en la calle: ${calle}.`
        };
        // Envio el correo
        const info = await transporter.sendMail(mailOptions);
      } else {
        console.log("NO Notifico por correo el reporte creado")
      }
    }
  }
}
